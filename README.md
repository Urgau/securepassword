# SecurePassword

----------

## À propos

----------
SecurePassword est un gestionnaire de mot de passe pour les personnes ayant envie de simplicité et de sécurité le plus simplement possible.

SecurePassword permet de sauvegarder le nom du site, l'identifiant, le mot de passe, l'email, la réponse à la question de sécurité et des notes par rapport au site. A terme il sera possible d'avoir des champs personnalisable. Il dispose aussi d'un générateur de mot de passe baser aléatoire de 32 caractères.

La base de donnée complète est toujours sauvegarder en [AES-256](https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard) avec une clé de 256 octets et pour éviter les corruption de la base de données SecurePassword utilise aussi [HMAC SHA-256](https://fr.wikipedia.org/wiki/Keyed-Hash_Message_Authentication_Code).

SecurePassword dispose aussi d'un "password strenght" baser sur le site [passwordmeter.com](http://www.passwordmeter.com)

## Installation

----------
SecurePassword peut être télécharger sur le site officiel (indisponible pour le moment) ou compiler. SecurePassword n'a pas besoin d'installation puisqu'il ne nécessite aucune bibliothèque supplémentaire, il est donc portable et exécutable sur n'importe quelle machine disposant du Framework .Net. Vous pouvez aussi compiler de code source avec Visual Studio.

## Reporter un bug

----------
Le dépôt Bitbucket dispose d'un gestionnaire de bug où vous pouvez reporter un bug. Il est disponible à cette adresse : [https://bitbucket.org/Urgau/securepassword/issues](https://bitbucket.org/Urgau/securepassword/issues)
