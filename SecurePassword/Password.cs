﻿using System;
using System.Text.RegularExpressions;

namespace SecurePassword
{
    class Password
    {
        public static string CreateRandomPassword(int passwordLength)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-#*-+.@{[]}";
            char[] chars = new char[passwordLength];
            Random rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        public static int PasswordStrenght(string password)
        {
            if (string.IsNullOrWhiteSpace(password)) return 0;

            /***************************/
            /********Baser sur :********/
            /*****passwordmeter.com*****/
            /***************************/

            int score = 0;

            //Number of Characters
            score += password.Length * 4;

            int countUpper = 0;
            int countLower = 0;
            int countNumber = 0;
            int countSymbols = 0;
            int countOther = 0;
            int countConsecutiveUpper = 0;
            int countConsecutiveLower = 0;
            int countConsecutiveNumber = 0;
            int countRepeatCaracters = 0;

            //For password[0]
            if (char.IsUpper(password[0])) countUpper++;
            else if (char.IsLower(password[0])) countLower++;
            else if (char.IsNumber(password[0])) countNumber++;
            else if (char.IsSymbol(password[0])) countSymbols++;
            else if (char.IsControl(password[0]) || char.IsPunctuation(password[0])) countOther++;

            for (int i = 1; i < password.Length; i++)
            {
                if (char.IsUpper(password[i]))
                {
                    countUpper++;
                    if (char.IsUpper(password[i - 1]))
                    {
                        countConsecutiveUpper++;
                        if (password[i] == password[i - 1]) countRepeatCaracters++;
                    }
                }
                else if (char.IsLower(password[i]))
                {
                    countLower++;
                    if (char.IsLower(password[i - 1]))
                    {
                        countConsecutiveLower++;
                        if (password[i] == password[i - 1]) countRepeatCaracters++;
                    }
                }
                else if (char.IsNumber(password[i]))
                {
                    countNumber++;
                    if (char.IsNumber(password[i - 1]))
                    {
                        countConsecutiveNumber++;
                        if (password[i] == password[i - 1]) countRepeatCaracters++;
                    }
                }
                else if (char.IsSymbol(password[i])) countSymbols++;
                else if (char.IsControl(password[i]) || char.IsPunctuation(password[i])) countOther++;
            }
            //Uppercase Letters
            score += (password.Length - countUpper) * 2;
            //Lowercase  Letters
            score += (password.Length - countLower) * 2;
            //Numbers
            score += countNumber * 4;
            //Symbols
            score += countSymbols * 6;
            //Others
            score += countOther * 3;

            //Letters Only
            score -= Regex.IsMatch(password, @"^[a-zA-Z]+$") ? password.Length : 0;
            //Numbers Only
            score -= Regex.IsMatch(password, @"^[0-9]+$") ? password.Length : 0;

            //Repeat Characters
            score -= countRepeatCaracters * 6;
            //Consecutive Uppercase
            score -= countConsecutiveUpper * 2;
            //Consecutive Lowercase
            score -= countConsecutiveLower * 2;
            //Consecutive Numbers
            score -= countConsecutiveNumber * 2;

            return score >= 200 ? 100 : score / 2;
        }
    }
}
