﻿using Microsoft.Win32;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace SecurePassword.Windows
{
    /// <summary>
    /// Logique d'interaction pour LoadDatabaseWindow.xaml
    /// </summary>
    public partial class LoadDatabaseWindow
    {
        public LoadDatabaseWindow()
        {
            InitializeComponent();
            pbPassword.Focus();
        }

        private void pbPassword_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnValider.Focus();
                LaunchApp();
            }
        }

        private void btnValider_Click(object sender, RoutedEventArgs e)
        {
            LaunchApp();
        }

        private void LaunchApp()
        {
            if (File.Exists(cbPath.Text) && !string.IsNullOrWhiteSpace(pbPassword.Password))
            {
                string password = Cryptographie.Md5Simple.Hash(pbPassword.Password);
                Database.DatabaseInformation di = new Database.DatabaseInformation(cbPath.Text, password);

                //Verif password
                if (Database.VerifyFile(di))
                {
                    //save history
                    Properties.Settings.Default.HistoryPath = new System.Collections.Specialized.StringCollection
                    {
                        cbPath.Text
                    };
                    foreach (string s2 in cbPath.Items)
                    {
                        if (!Properties.Settings.Default.HistoryPath.Contains(s2))
                            Properties.Settings.Default.HistoryPath.Add(s2);
                    }
                    Properties.Settings.Default.Save();

                    MainWindow mw = new MainWindow(di);
                    mw.Show();
                    Close();
                }
                else
                {
                    MessageBox.Show("Ce n'est pas le bon mot de passe ou le fichier à été corrompu !", "SecurePassword - Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else if (!File.Exists(cbPath.Text))
            {
                MessageBox.Show("Le fichier spécifier n'existe pas !", "SecurePassword - Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (string.IsNullOrWhiteSpace(pbPassword.Password))
            {
                MessageBox.Show("Le mot de passe est vide !", "SecurePassword - Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnPath_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            var showDialog = ofd.ShowDialog();
            if (showDialog != null && showDialog.Value)
            {
                cbPath.SelectedIndex = cbPath.Items.Add(ofd.FileName);
            }
        }

        private void btnNewDatabase_Click(object sender, RoutedEventArgs e)
        {
            NewDatabaseWindow ndbw = new NewDatabaseWindow();
            ndbw.Show();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.HistoryPath != null)
            {
                foreach (string s in Properties.Settings.Default.HistoryPath)
                    cbPath.Items.Add(s);
            }

            cbPath.SelectedIndex = cbPath.Items.Count > 0 ? 0 : -1;
        }

        private void cbPathContextMenuRemove_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Voulez vous supprimer " + cbPath.Text + " ?", "SecurePassword", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                cbPath.Items.Remove(cbPath.Text);
                cbPath.SelectedIndex = cbPath.Items.Count > 0 ? 0 : -1;
            }
        }
    }
}
