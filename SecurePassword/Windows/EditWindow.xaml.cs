﻿using System;
using System.Windows;

namespace SecurePassword.Windows
{
    /// <summary>
    /// Logique d'interaction pour EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {
        private string ID;

        public EditWindow(string id)
        {
            InitializeComponent();

            WebsiteInformation wi = Core.GetWebsiteInformation(id);
            ID = wi.Id;
            tbName.Text = wi.Name;
            tbUrl.Text = wi.Url;
            tbUsername.Text = wi.Username;
            pbPassword.Password = wi.Password;
            tbEmail.Text = wi.Email;
            tbSecurityAnswer.Text = wi.SecurityAnswer;
            tbNotes.Text = wi.Notes;
        }

        private void btnNewPassword_Click(object sender, RoutedEventArgs e)
        {
            pbPassword.Password = Password.CreateRandomPassword(32);
        }

        private void pbPassword_PasswordChanged(object sender, EventArgs e)
        {
            pbPasswordStrenght.Value = Password.PasswordStrenght(pbPassword.Password);
        }

        private bool IsSave = false;
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbName.Text))
            {
                WebsiteInformation wi = new WebsiteInformation
                {
                    Id = ID,
                    Name = tbName.Text,
                    Url = tbUrl.Text,
                    Username = tbUsername.Text,
                    Password = pbPassword.Password,
                    Email = tbEmail.Text,
                    SecurityAnswer = tbSecurityAnswer.Text,
                    Notes = tbNotes.Text
                };

                Core.UpdateWebsite(wi);
                IsSave = true;
                Close();
            }
            else
            {
                MessageBox.Show("Vous devez obligatoirement mettre un nom au site !", "SecurePassword", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbName.Text))
            {
                if (MessageBox.Show("Etez-vous sûr de ne pas vouloir garder ces modifications ?", "SecurePassword", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    IsSave = true;
                    Close();
                }
            }
            else
            {
                MessageBox.Show("Vous devez obligatoirement mettre un nom au site !", "SecurePassword", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!IsSave)
            {
                if (!string.IsNullOrWhiteSpace(tbName.Text))
                {
                    if (MessageBox.Show("Etez-vous sûr de ne pas vouloir garder ces modifications ?", "SecurePassword", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    e.Cancel = true;
                    MessageBox.Show("Vous devez obligatoirement mettre un nom au site !", "SecurePassword", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }
    }
}
