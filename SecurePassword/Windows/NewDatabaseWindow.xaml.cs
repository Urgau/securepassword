﻿using Microsoft.Win32;
using System;
using System.Windows;

namespace SecurePassword.Windows
{
    /// <summary>
    /// Logique d'interaction pour NewDatabaseWindow.xaml
    /// </summary>
    public partial class NewDatabaseWindow
    {
        public NewDatabaseWindow()
        {
            InitializeComponent();
        }

        private void btnValider_Click(object sender, RoutedEventArgs e)
        {
            if (pbMdp.Password != pbMdpRepete.Password)
            {
                MessageBox.Show("Les mots de passe ne sont pas les mêmes !");
            }
            else if (string.IsNullOrWhiteSpace(tbPath.Text))
            {
                MessageBox.Show("Vous avez oubliez de renseignez la destinnation de la base de donnée !");
            }
            else
            {
                string password = Cryptographie.Md5Simple.Hash(pbMdp.Password);
                if (Database.Create(new Database.DatabaseInformation(tbPath.Text, password)))
                {
                    MessageBox.Show("La base de données à été créer avec succes !", "SecurePassword", MessageBoxButton.OK);
                    Close();
                }
                else
                {
                    MessageBox.Show("Une erreur c'est produit lors de création de la base de donnée", "SecurePassword", MessageBoxButton.OK);
                }
            }
        }

        private void btnPath_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                DefaultExt = ".db",
                Title = "SecurePassword - Choix du dossier d'enregistrement",
                Filter = "SecurePassword Database (.db)|*.db|All Files (*.*)|*.*"
            };
            if (saveFileDialog.ShowDialog() == true)
                tbPath.Text = saveFileDialog.FileName;
        }

        private void pbMdp_PasswordChanged(object sender, EventArgs e)
        {
            pbPasswordStrenght.Value = Password.PasswordStrenght(pbMdp.Password);
        }
    }
}
