﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;

namespace SecurePassword
{
    public class Database
    {
        public static bool Create(DatabaseInformation di)
        {
            return Write(di, new List<WebsiteInformation>());
        }

        public static bool Save(DatabaseInformation di, List<WebsiteInformation> websites)
        {
            return Write(di, websites);
        }

        public static List<WebsiteInformation> Load(DatabaseInformation di)
        {
            using (FileStream fs = new FileStream(di.Path, FileMode.Open))
            {
                //Recuper HMAC
                byte[] hash = new byte[32];
                fs.Read(hash, 0, hash.Length);
                //Console.WriteLine("Load hash : " + ToHexString(hash));

                using (AesManaged aes = new AesManaged() { KeySize = 256, BlockSize = 128, IV = Encoding.Default.GetBytes("qQ9s~5i6@GH$X#z2") })
                {
                    aes.Key = Encoding.Default.GetBytes(di.AesKey);
                    using (ICryptoTransform decryptor = aes.CreateDecryptor())
                    {
                        using (CryptoStream cs = new CryptoStream(fs, decryptor, CryptoStreamMode.Read))
                        {
                            XmlSerializer xml = new XmlSerializer(typeof(List<WebsiteInformation>));
                            return (List<WebsiteInformation>)xml.Deserialize(cs);
                        }
                    }
                }
            }
        }

        public static bool VerifyFile(DatabaseInformation di)
        {
            bool err = false;
            // Initialize the keyed hash object. 
            using (HMACSHA256 hmac = new HMACSHA256(Encoding.Default.GetBytes(di.AesKey)))
            {
                // Create an array to hold the keyed hash value read from the file.
                byte[] storedHash = new byte[hmac.HashSize / 8];
                //Console.WriteLine(Encoding.Default.GetString(storedHash));
                // Create a FileStream for the source file.
                using (FileStream inStream = new FileStream(di.Path, FileMode.Open))
                {
                    // Read in the storedHash.
                    inStream.Read(storedHash, 0, storedHash.Length);
                    //Console.WriteLine("Verif hash : " + ToHexString(storedHash));
                    // Compute the hash of the remaining contents of the file.
                    // The stream is properly positioned at the beginning of the content, 
                    // immediately after the stored hash value.
                    byte[] computedHash = hmac.ComputeHash(inStream);
                    //Console.WriteLine("Verif2 hash : " + ToHexString(computedHash));
                    // compare the computed hash with the stored value

                    for (int i = 0; i < storedHash.Length; i++)
                    {
                        if (computedHash[i] != storedHash[i])
                        {
                            err = true;
                            break;
                        }
                    }
                }
            }

            return !err;
        }

        private static bool Write(DatabaseInformation di, List<WebsiteInformation> websites)
        {
            try
            {
                byte[] key = Encoding.Default.GetBytes(di.AesKey);

                using (AesManaged aes = new AesManaged() { KeySize = 256, BlockSize = 128, IV = Encoding.Default.GetBytes("qQ9s~5i6@GH$X#z2") })
                {
                    aes.Key = key;
                    using (ICryptoTransform encryptor = aes.CreateEncryptor())
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                            {
                                //Etape 1 - encrypter les données
                                XmlSerializer xml = new XmlSerializer(typeof(List<WebsiteInformation>));
                                xml.Serialize(cs, websites);
                                cs.FlushFinalBlock();

                                //Etape 2 - Determiner le hash
                                ms.Position = 0; //Pour aller au tous debut du flux
                                byte[] hash = new HMACSHA256(key).ComputeHash(ms);

                                //Etape 3 - Ecrire le tous dans un fichier
                                //Write into file
                                using (FileStream fs = new FileStream(di.Path, FileMode.Create))
                                {
                                    //Write hash
                                    fs.Write(hash, 0, hash.Length);

                                    //Write data
                                    byte[] buffer = ms.ToArray();
                                    fs.Write(buffer, 0, buffer.Length);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(@"Database.Write : " + ex.ToString());
                return false;
            }

            return File.Exists(di.Path);
        }

/*
        private static string ToHexString(byte[] hex)
        {
            if (hex == null) return null;
            if (hex.Length == 0) return string.Empty;

            StringBuilder s = new StringBuilder();
            foreach (byte b in hex)
            {
                s.Append(b.ToString("x2"));
            }
            return s.ToString();
        }
*/

        public class DatabaseInformation
        {
            public string Path { get; private set; }
            public string AesKey { get; private set; }

            public DatabaseInformation(string path, string key)
            {
                Path = path; AesKey = key;
            }
        }
    }
}
