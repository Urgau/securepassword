﻿using SecurePassword.Windows;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using static SecurePassword.Database;

namespace SecurePassword
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        string _websiteInformationShowId = "";

        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(DatabaseInformation di)
        {
            InitializeComponent();

            try
            {
                Core.LoadDatabase(di);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(0);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Core.HaveDatabase)
            {
                lbSites.ItemsSource = Core.ListWebsite();
                AfficherPremierOuNull();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Voulez-vous enregsitrez avant de quitter le logiciel ?", "SecurePassword", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Core.SaveDatabase();
            }
        }

        private void ShowWebsiteInformation(string id)
        {
            WebsiteInformation wi;
            if (id != "null")
            {
                wi = Core.GetWebsiteInformation(id);
                spWebsiteInterface.IsEnabled = true;
            }
            else
            {
                wi = WebsiteInformation.Null;
                spWebsiteInterface.IsEnabled = false;
            }
            _websiteInformationShowId = wi.Id;
            tbWebsiteName.Text = wi.Name;

            //Url
            if (string.IsNullOrWhiteSpace(wi.Url))
            {
                separator1.Visibility = Visibility.Collapsed;
                gridWebsiteUrl.Visibility = Visibility.Collapsed;
            }
            else
            {
                separator1.Visibility = string.IsNullOrWhiteSpace(wi.Username) ? Visibility.Collapsed : Visibility.Visible;
                gridWebsiteUrl.Visibility = Visibility.Visible;

                tbWebsiteUrl.Text = wi.Url;
            }

            //Username
            if (string.IsNullOrWhiteSpace(wi.Username))
            {
                separator2.Visibility = Visibility.Collapsed;
                gridWebsiteUsername.Visibility = Visibility.Collapsed;
            }
            else
            {
                separator2.Visibility = string.IsNullOrWhiteSpace(wi.Password) ? Visibility.Collapsed : Visibility.Visible;
                gridWebsiteUsername.Visibility = Visibility.Visible;

                tbWebsiteUsername.Text = wi.Username;
            }

            //Password
            if (string.IsNullOrWhiteSpace(wi.Password))
            {
                separator3.Visibility = Visibility.Collapsed;
                gridWebsitePassword.Visibility = Visibility.Collapsed;
            }
            else
            {
                separator3.Visibility = string.IsNullOrWhiteSpace(wi.Email) ? Visibility.Collapsed : Visibility.Visible;
                gridWebsitePassword.Visibility = Visibility.Visible;

                tbWebsitePassword.Password = wi.Password;
            }

            //Email
            if (string.IsNullOrWhiteSpace(wi.Email))
            {
                separator4.Visibility = Visibility.Collapsed;
                gridWebsiteEmail.Visibility = Visibility.Collapsed;
            }
            else
            {
                separator4.Visibility = string.IsNullOrWhiteSpace(wi.SecurityAnswer) ? Visibility.Collapsed : Visibility.Visible;
                gridWebsiteEmail.Visibility = Visibility.Visible;

                tbWebsiteEmail.Text = wi.Email;
            }

            //Security answer
            if (string.IsNullOrWhiteSpace(wi.SecurityAnswer))
            {
                separator5.Visibility = Visibility.Collapsed;
                gridWebsiteSecurityAnswer.Visibility = Visibility.Collapsed;
            }
            else
            {
                separator5.Visibility = string.IsNullOrWhiteSpace(wi.Notes) ? Visibility.Collapsed : Visibility.Visible;
                gridWebsiteSecurityAnswer.Visibility = Visibility.Visible;

                tbWebsiteSecurityAnswer.Text = wi.SecurityAnswer;
            }

            //Notes
            if (string.IsNullOrWhiteSpace(wi.Notes))
            {
                gridWebsiteNotes.Visibility = Visibility.Collapsed;
                separator5.Visibility = Visibility.Collapsed;
            }
            else
            {
                separator5.Visibility = Visibility.Visible;
                gridWebsiteNotes.Visibility = Visibility.Visible;

                tbWebsiteNotes.Text = wi.Notes;
            }
        }

        private void lbSites_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbSites.SelectedIndex != -1)
            {
                spWebsiteInterface.IsEnabled = true;
                ShowWebsiteInformation(((Website)e.AddedItems[0]).Id);
            }
            else
            {
                spWebsiteInterface.IsEnabled = false;
            }
        }

        private void tbRechercher_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.Changes.Count > 0 && Core.HaveDatabase)
            {
                if (string.IsNullOrWhiteSpace(tbRechercher.Text))
                {
                    lbSites.ItemsSource = Core.ListWebsite();
                }
                else if (tbRechercher.Text != "Rechercher")
                {
                    lbSites.ItemsSource = Core.ListWebsite(tbRechercher.Text);
                }
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            string id = Core.AddWebsite();
            EditWindow edit = new EditWindow(id);
            edit.ShowDialog();
            ShowWebsiteInformation(id);
            lbSites.ItemsSource = Core.ListWebsite();
        }

        private void miNewDatabase_Click(object sender, RoutedEventArgs e)
        {
            NewDatabaseWindow ndbw = new NewDatabaseWindow();
            ndbw.Show();
        }

        private void miLoadDatabase_Click(object sender, RoutedEventArgs e)
        {
            LoadDatabaseWindow ldbw = new LoadDatabaseWindow();
            ldbw.Show();
            Close();
        }

        private void miSaveDatabase_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(Core.SaveDatabase()
                ? "La base de donnée à bien été sauvegarder !"
                : "Une erreur est survenue lors de la sauvegarde de la base de donnée !");
        }

        private void miExitApp_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Voulez-vous enregsitrez avant de quitter le logiciel ?", "SecurePassword", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Core.SaveDatabase();
            }
            Close();
        }

        private void miCredits_Click(object sender, RoutedEventArgs e)
        {
            const string license = "The MIT License (MIT)\n" +
                                    "\n" +
                                    "Copyright(c) 2016 Urgau\n" +
                                    "\n" +
                                    "L'autorisation est accordée, gracieusement, à toute personne acquérant une copie de cette bibliothèque et des fichiers de documentation associés (la \"Bibliothèque\"), de commercialiser la Bibliothèque sans restriction, notamment les droits d'utiliser, de copier, de modifier, de fusionner, de publier, de distribuer, de sous-licencier et / ou de vendre des copies de la Bibliothèque, ainsi que d'autoriser les personnes auxquelles la Bibliothèque est fournie à le faire, sous réserve des conditions suivantes :\n" +
                                    "\n" +
                                    "La déclaration de copyright ci-dessus et la présente autorisation doivent être incluses dans toutes copies ou parties substantielles de la Bibliothèque.\n" +
                                    "\n" +
                                    "LA BIBLIOTHÈQUE EST FOURNIE \"TELLE QUELLE\", SANS GARANTIE D'AUCUNE SORTE, EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE, D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN AUCUN CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT RESPONSABLES DE TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ, QUE CE SOIT DANS LE CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN PROVENANCE DE, CONSÉCUTIF À OU EN RELATION AVEC LA BIBLIOTHÈQUE OU SON UTILISATION, OU AVEC D'AUTRES ÉLÉMENTS DE LA BIBLIOTHÈQUE.";

            MessageBox.Show(license + "\n\nIcons : Designed by Madebyoliver (http://www.flaticon.com/authors/madebyoliver) and distributed by Flaticon !\nPassword strenght baser sur : passwordmeter.com");
        }

        private void miAbout_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("SecurePassword v0.2\n\nLicense in Credits");
        }

        private void lbContextMenuCopyUsername_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(Core.GetWebsiteInformation(((Website)lbSites.Items[lbSites.SelectedIndex]).Id).Username);
        }

        private void lbContextMenuCopyPassword_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(Core.GetWebsiteInformation(((Website)lbSites.Items[lbSites.SelectedIndex]).Id).Password);
        }

        private void lbContextMenuRemove_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Etez-vous sûr de vouloir supprimer ce site ?", "Suppresion de site", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                Core.RemoveWebsite(((Website)lbSites.Items[lbSites.SelectedIndex]).Id);
                lbSites.ItemsSource = Core.ListWebsite();
                AfficherPremierOuNull();
            }
        }

        private void btnEditWebsite_Click(object sender, RoutedEventArgs e)
        {
            EditWindow ew = new EditWindow(_websiteInformationShowId);
            ew.ShowDialog();
            ShowWebsiteInformation(_websiteInformationShowId);
        }

        private void btnWebsiteUrl_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(tbWebsiteUrl.Text.StartsWith("http://") ? tbWebsiteUrl.Text : "http://" + tbWebsiteUrl.Text);
        }

        private void btnWebsiteUsername_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(tbWebsiteUsername.Text);
        }

        private void btnWebsitePassword_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(tbWebsitePassword.Password);
        }

        private void btnWebsiteEmail_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(tbWebsiteEmail.Text);
        }

        private void btnWebsiteSecurityAnswer_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(tbWebsiteSecurityAnswer.Text);
        }

        private void btnWebsiteNotes_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(tbWebsiteNotes.Text);
        }

        private void btnRemoveWebsite_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Etez-vous sûr de vouloir supprimer ce site ?", "Suppresion de site", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                Core.RemoveWebsite(_websiteInformationShowId);
                lbSites.ItemsSource = Core.ListWebsite();
                AfficherPremierOuNull();
            }
        }

        private void AfficherPremierOuNull()
        {
            if (lbSites.Items.Count > 0)
            {
                lbSites.SelectedIndex = 0;
            }
            else
            {
                ShowWebsiteInformation("null");
            }
        }
    }
}
