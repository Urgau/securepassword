﻿using System.Collections.Generic;

namespace SecurePassword
{
    class Core
    {
        public static Database.DatabaseInformation DatabaseInformation { get; private set; }
        public static List<WebsiteInformation> Websites { get; private set; }
        public static bool HaveDatabase { get; private set; }

        public static void LoadDatabase(Database.DatabaseInformation di)
        {
            if (di != null)
            {
                DatabaseInformation = di;
                Websites = Database.Load(DatabaseInformation);
                HaveDatabase = true;
            }
            else
            {
                HaveDatabase = false;
            }
        }

        public static bool SaveDatabase()
        {
            if (HaveDatabase)
                return DatabaseInformation != null && Database.Save(DatabaseInformation, Websites);
            return false;
        }

        public static List<Website> ListWebsite()
        {
            List<Website> websites = new List<Website>();

            foreach (WebsiteInformation wi in Websites)
            {
                websites.Add(new Website(wi.Id, wi.Name) { UrlIcone = "http://www.google.com/s2/favicons?domain=" + wi.Url });
            }

            return websites;
        }

        public static List<Website> ListWebsite(string contains)
        {
            List<Website> websites = new List<Website>();

            foreach (WebsiteInformation wi in Websites)
            {
                if (wi.Name.Contains(contains))
                {
                    websites.Add(new Website(wi.Id, wi.Name) { UrlIcone = "http://www.google.com/s2/favicons?domain=" + wi.Url });
                }
            }

            return websites;
        }

        public static WebsiteInformation GetWebsiteInformation(string id)
        {
            foreach (WebsiteInformation wi in Websites)
            {
                if (wi.Id == id)
                {
                    return wi;
                }
            }

            return WebsiteInformation.Null;
        }

        public static string AddWebsite()
        {
            string id = Password.CreateRandomPassword(5);
            Websites.Add(new WebsiteInformation() { Id = id, Name = "No name" });

            return id;
        }

        public static bool UpdateWebsite(WebsiteInformation wi)
        {
            for (int i = 0; i < Websites.Count; i++)
            {
                /*Console.WriteLine("wi.ID = \"" + wi.ID + "\"");
                Console.WriteLine("Websites[" + i + "].ID = " + Websites[i].ID);*/
                if (Websites[i].Id == wi.Id)
                {
                    Websites[i] = wi;
                    return true;
                }
            }
            return false;
        }

        public static void RemoveWebsite(string id)
        {
            for (int i = 0; i < Websites.Count; i++)
            {
                if (Websites[i].Id == id)
                {
                    Websites.RemoveAt(i);
                    break;
                }
            }
        }
    }
}
