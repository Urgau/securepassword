﻿using System.Windows;

namespace SecurePassword.UserControl
{
    /// <summary>
    /// Logique d'interaction pour PasswordBlock.xaml
    /// </summary>
    public partial class PasswordBlock
    {
        public PasswordBlock()
        {
            InitializeComponent();
        }

        public bool IsShow { get; set; }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { _password = value;
                tbPassword.Text = IsShow ? _password : new string('•', _password.Length);
            }
        }

        private void BtnShowPassword_OnClick(object sender, RoutedEventArgs e)
        {
            IsShow = true;
            btnShowPassword.Visibility = Visibility.Hidden;
            btnHidePassword.Visibility = Visibility.Visible;
            tbPassword.Text = _password;
        }

        private void BtnHidePassword_OnClick(object sender, RoutedEventArgs e)
        {
            IsShow = false;
            btnShowPassword.Visibility = Visibility.Visible;
            btnHidePassword.Visibility = Visibility.Hidden;
            tbPassword.Text = new string('•', _password.Length);
        }
    }
}
