﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace SecurePassword.UserControl
{
    /// <summary>
    /// Logique d'interaction pour PasswordBox.xaml
    /// </summary>
    public partial class PasswordBox
    {
        public PasswordBox()
        {
            InitializeComponent();
        }

        private string _password;
        private bool _pbPasswordActiveChangedPassword = true;
        private bool _tbPasswordActiveChangedPassword = true;

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                tbPassword.Text = value;
                pbPassword.Password = value; _tbPasswordActiveChangedPassword = false; //Juste un event
            }
        }
        public delegate void DPasswordChanged(object sender, EventArgs e);
        public event DPasswordChanged PasswordChanged;

        private void btnShowPassword_Click(object sender, RoutedEventArgs e)
        {
            tbPassword.IsEnabled = true;
            tbPassword.Visibility = Visibility.Visible;
            btnHidePassword.IsEnabled = true;
            btnHidePassword.Visibility = Visibility.Visible;

            pbPassword.IsEnabled = false;
            pbPassword.Visibility = Visibility.Hidden;
            btnShowPassword.IsEnabled = false;
            btnShowPassword.Visibility = Visibility.Hidden;

            tbPassword.Text = Password; _tbPasswordActiveChangedPassword = false;
            tbPassword.Focus();
        }

        private void btnHidePassword_Click(object sender, RoutedEventArgs e)
        {
            tbPassword.IsEnabled = false;
            tbPassword.Visibility = Visibility.Hidden;
            btnHidePassword.IsEnabled = false;
            btnHidePassword.Visibility = Visibility.Hidden;

            pbPassword.IsEnabled = true;
            pbPassword.Visibility = Visibility.Visible;
            btnShowPassword.IsEnabled = true;
            btnShowPassword.Visibility = Visibility.Visible;

            pbPassword.Password = Password; _pbPasswordActiveChangedPassword = false;
            pbPassword.Focus();
        }

        private void pbPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            _password = pbPassword.Password;
            if (_pbPasswordActiveChangedPassword)
            {
                PasswordChanged?.Invoke(this, null);
            }
            else
            {
                _pbPasswordActiveChangedPassword = true;
            }
        }

        private void tbPassword_TextChanged(object sender, TextChangedEventArgs e)
        {
            _password = tbPassword.Text;
            if (_tbPasswordActiveChangedPassword)
            {
                PasswordChanged?.Invoke(this, null);
            }
            else
            {
                _tbPasswordActiveChangedPassword = true;
            }
        }
    }
}
