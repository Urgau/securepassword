﻿namespace SecurePassword
{
    public class Website
    {
        public string Name { get; set; }
        public string UrlIcone { get; set; }
        public string Id { get; private set; }

        public Website(string id, string name)
        {
            Id = id; Name = name;
        }
    }

    public class WebsiteInformation
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string SecurityAnswer { get; set; }
        public string Notes { get; set; }
        public string Id { get; set; }

        public static WebsiteInformation Null => new WebsiteInformation()
        {
            Name = "Nom du site",
            Url = "exemple.com",
            Username = "identifiant",
            Password = "erfnh^3;9ù;m$,ioYUDHIDHIUl$um;io84943p9^;ù4p9im;pm",
            Notes = "Mes notes par raport au site",
            Id = "null"
        };
    }
}
