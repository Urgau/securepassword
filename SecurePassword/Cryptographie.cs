﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SecurePassword
{
    public class Cryptographie
    {
        public class AesSimple
        {
            private AesManaged AesManaged { get; set; }
            private ICryptoTransform Encryptor { get; set; }
            private ICryptoTransform Decryptor { get; set; }

            public AesSimple(string key)
            {
                AesManaged = new AesManaged() { KeySize = 256, BlockSize = 128, Mode = CipherMode.CBC };

                if (key.Length != AesManaged.KeySize / 8) { throw new Exception(
                    $"La Key doit être de {AesManaged.KeySize/8} caractères"); }
                //if (iv.Length != AesManaged.BlockSize / 8) { throw new Exception(string.Format("L'IV doit être de {0} caractères", AesManaged.BlockSize / 8)); }
                AesManaged.Key = Encoding.Default.GetBytes(key);
                AesManaged.IV = Encoding.Default.GetBytes("qQ9s~5i6@GH$X#z2");

                Encryptor = AesManaged.CreateEncryptor();
                Decryptor = AesManaged.CreateDecryptor();
            }

            public string Crypt(string text)
            {
                if (!string.IsNullOrWhiteSpace(text))
                {
                    byte[] bytesText = Encoding.Default.GetBytes(text);
                    byte[] encrypt = Encryptor.TransformFinalBlock(bytesText, 0, bytesText.Length);

                    return Encoding.Default.GetString(encrypt);
                }
                else
                {
                    return "";
                }
            }

            public string Decrypt(string encrypted)
            {
                if (!string.IsNullOrWhiteSpace(encrypted))
                {
                    byte[] bytesEncrypted = Encoding.Default.GetBytes(encrypted);
                    byte[] decrypt = Decryptor.TransformFinalBlock(bytesEncrypted, 0, bytesEncrypted.Length);

                    return Encoding.Default.GetString(decrypt);
                }
                else
                {
                    return "";
                }
            }
        }

        public class Md5Simple
        {
            public static string Hash(string text)
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(text));
                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                foreach (byte d in data)
                {
                    sBuilder.Append(d.ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }
    }
}
